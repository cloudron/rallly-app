[0.1.0]
* Initial version with revision e4482a

[0.2.0]
* Update Rallly to 6e7b332d

[1.0.0]
* Update Rallly to 6332d6459

[1.0.1]
* Update Rallly to fcb2d362

[1.1.0]
* Update Rallly to 2.0.0

[1.2.0]
* Update Rallly to 2.1.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.1.0)
* Improve usability of mobile poll (#486) [8c74836]
* Increase timeout when install dependencies [bcf4e0b]
* Update crowdin config [c20002e]
* New translations app.json (Swedish) (#487) [f6d3aed]

[1.2.1]
* Update Rallly to 2.1.1
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.1.1)
* Increase participant page max width [4811fbe]
* Bring back showing the day of the week for date options (#499) [0d805c8]

[1.3.0]
* Update Rally to 2.2.3
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.2.0)
* Allow participant to enter email to receive edit link (#534) [0ac3c95]
* Updated workflow for adding and updating participants (#500) [5d7db84]
* Refactor email templating code (#533) [309cb10]
* Switch to turborepo (#532) [0a836ae]
* Use built-in next/font instead of @next/font [6682c53]
* Update trpcs routes (#531) [18eca7c]
* Improve time slot start times (#519) [da895c1]
* Update submit button text (#522) [d66663a]
* Get urlId during ssr (#521) [4ee3d7c]
* Lazy load animation library to help reduce bundle size (#502) [696cd44]
* Use nextjs layout feature [c2c000f]
* Fix margin on admin controls [02ef900]
* Add `LANDING_PAGE` config option [a661630]
* Remove page animations [ecd63ae]
* Fix framer motion missing layout animations [aab9995]
* Prevent form submission when poll is locked [e96c31b]

[1.4.0]
* Update Rallly to 2.3.1
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.3.1)
* Allow users to log in with magic link (#553) [2cf9ad4]
* Update notification flow (#548) [39a0755]
* Add dropdown indicator to language selector (#552) [5b78093]
* Remove custom Crowdin commit message [2ebda9f]
* Update email subject lines [611d04e]
* Make submitting a participant a bit faster [e06c55c]
* Fix crash in week view (#549) [1d31a42]
* Fix crowdin integration [9fe794a]
* Include pollId when capturing events by @lukevella in https://github.com/lukevella/rallly/pull/555
* Allow SMTP server to be configured without auth by @lukevella in https://github.com/lukevella/rallly/pull/556
* Update translations by @lukevella in https://github.com/lukevella/rallly/pull/554

[1.5.0]
* Update Rallly to 2.4.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.4.0)
* Added new security options for self-hosters
* Add config to secure instance from unauth users by @lukevella in #559
* Update build command by @lukevella in #560
* Temporarily disable arm64 builds by @lukevella in #561
* Update translations by @lukevella in #558
* Hide comment dropdown menu when unavailable to user by @jonas-hoebenreich in #564
* Remove dead verification code by @lukevella in #566
* Update email templates by @lukevella in #562
* Update translations by @lukevella in #565

[1.5.1]
* Update Rallly to 2.4.1
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.4.1)
* Capture participant email in posthog by @lukevella in https://github.com/lukevella/rallly/pull/569
* Update translations by @lukevella in https://github.com/lukevella/rallly/pull/567
* Fix `DISABLE_LANDING_PAGE` not working by @lukevella in https://github.com/lukevella/rallly/pull/572

[1.6.0]
* Update Rallly to 2.5.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.5.0)
* The participant dropdown menu now has an option to change a participant's name.

[1.6.1]
* Update Rallly to 2.5.1
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.5.1)
* Update translations by @lukevella in https://github.com/lukevella/rallly/pull/579
* Update translations by @lukevella in https://github.com/lukevella/rallly/pull/580
* Add Vietnamese Language by @lukevella in https://github.com/lukevella/rallly/pull/581
* Update translations by @lukevella in https://github.com/lukevella/rallly/pull/582
* Update translations by @lukevella in https://github.com/lukevella/rallly/pull/585
* Conditionally initialize posthog by @lukevella in https://github.com/lukevella/rallly/pull/586

[1.7.0]
* Update Rallly to 2.6.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.6.0)
* Update translations by @lukevella in https://github.com/lukevella/rallly/pull/589
* Add a feedback form by @lukevella in https://github.com/lukevella/rallly/pull/590
* Update translations by @lukevella in https://github.com/lukevella/rallly/pull/592
* Update translations by @lukevella in https://github.com/lukevella/rallly/pull/593
* Highlight option with highest score by @lukevella in https://github.com/lukevella/rallly/pull/595
* Update translations by @lukevella in https://github.com/lukevella/rallly/pull/594
* Update translations by @lukevella in https://github.com/lukevella/rallly/pull/596

[1.8.0]
* Update Rallly to 2.7.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.7.0)
* Quicker poll creation for logged in users
* Show poll status in list
* Fix error thrown in handler when changing name by @lukevella in #598
* Increase email send rate by @lukevella in #599
* Enable zooming and scaling by @jonas-hoebenreich in #600
* Skip user details step for logged in users by @lukevella in #602
* Update translations by @lukevella in #603
* Update translations by @lukevella in #604
* Show version number in app by @arcticFox-git in #601
* Update translations by @lukevella in #609
* Show poll status in list by @lukevella in #610

[1.8.1]
* Update Rallly to 2.7.1
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.7.1)
* Fix links not styled correctly by @lukevella in https://github.com/lukevella/rallly/pull/612

[1.9.0]
* Update Rallly to 2.8.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.8.0)
* Remove transition-all to remove weird transition by @TheZoker in https://github.com/lukevella/rallly/pull/614
* Update color palette by @lukevella in https://github.com/lukevella/rallly/pull/613
* Drop authorName from polls table by @lukevella in https://github.com/lukevella/rallly/pull/616
* Update translations by @lukevella in https://github.com/lukevella/rallly/pull/615
* Add Mintlify Docs Setup by @hanywang2 in https://github.com/lukevella/rallly/pull/617
* Add db seed script by @lukevella in https://github.com/lukevella/rallly/pull/619
* Update translations by @lukevella in https://github.com/lukevella/rallly/pull/618

[1.9.1]
* Update Rallly to 2.8.1
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.8.1)
* Make email placeholders RFC 2606 compliant by @peterlewis in https://github.com/lukevella/rallly/pull/622
* Fix malformed RFC 2606 tweak by @peterlewis in https://github.com/lukevella/rallly/pull/629
* Update translations by @lukevella in https://github.com/lukevella/rallly/pull/631

[1.9.2]
* Update Rallly to 2.8.3
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.8.3)
* This patch fixes an issue in the previous version where the wrong times were being stored in the database due to an automatic date conversion. We now make sure all times are stored in UTC and do the appropriate conversions on the client to show the correct times.
* Fixes an issue introduced in the previous patch where if a user deleted a date option, it would reset the votes for other date options as well.

[1.10.0]
* Update Rallly to 2.9.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.9.0)
* Create backend package by @lukevella in https://github.com/lukevella/rallly/pull/643
* Create languages package by @lukevella in https://github.com/lukevella/rallly/pull/644
* Use synchronous id generation by @lukevella in https://github.com/lukevella/rallly/pull/645
* Add icons package by @lukevella in https://github.com/lukevella/rallly/pull/646
* Use named exports for icons by @lukevella in https://github.com/lukevella/rallly/pull/647
* Wait for session to save by @lukevella in https://github.com/lukevella/rallly/pull/651
* Update email subject lines by @lukevella in https://github.com/lukevella/rallly/pull/652
* Improve email rendering speed by @lukevella in https://github.com/lukevella/rallly/pull/654

[1.10.1]
* Update Rallly to 2.9.1
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.9.1)
* Update translations by @lukevella in https://github.com/lukevella/rallly/pull/638
* Update translations by @lukevella in https://github.com/lukevella/rallly/pull/656
* Stop using localized links to app by @lukevella in https://github.com/lukevella/rallly/pull/657

[1.10.2]
* Update Rallly to 2.9.2
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.9.2)
* Update translations by @lukevella in https://github.com/lukevella/rallly/pull/658
* Improve query invalidation by @lukevella in https://github.com/lukevella/rallly/pull/659

[1.11.0]
* Update Rallly to 2.10.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.10.0)
* Added SMTP_TLS_ENABLED environment variable
* Fixed a bug that caused some requests to be serviced by our middleware unnecessarily

[1.12.0]
* Update Rallly to 2.11.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.11.0)
* New config for setting a no-reply email
* Updated email templates styling and content

[1.12.1]
* Update Rallly to 2.11.1
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v2.11.1)
* This patch fixes an issue that caused the incorrect end time to be displayed when setting the time zone to UTC.

[1.13.0]
* Update Rallly to 3.0.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.0.0)
* The AUTH_REQUIRED parameter has been removed. As of version 3, users must be logged in to create and manage polls. To further avoid use of your instance by unauthorized users, you are encouraged to use the ALLOWED_EMAILS parameter to limit who can register and login on your instance.
* Poll finalization
* Custom poll settings (hide participant list, hide scores, disable comments)
* Poll duplication

[1.14.0]
* Update Rallly to 3.0.1
* This update correctly sets the selfhosting mode for rallly so a few features are unlocked now
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.0.1)
* Show participant email in dropdown by @lukevella in #855
* Fix allowed email address matcher by @lukevella in #858
* Streamline env variable usage and set the app in self-hosting mode

[1.15.0]
* Update Rallly to 3.1.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.1.0)
* Added a new option to make the email field required for participants.
* Participant emails are visible in the participant dropdown menu.
* Allow making email required by @lukevella in #864
* Clear session store when leaving new page by @lukevella in #871

[1.16.0]
* Update base image to 4.2.0

[1.17.0]
* Update Rallly to 3.2.1
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.2.1)

[1.18.0]
* Update Rallly to 3.3.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.3.0)
* This update includes bug fixes and improvements under the hood.

[1.19.0]
* Update Rallly to 3.4.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.4.0)

[1.20.0]
* Enable OIDC login

[1.20.1]
* Update Rallly to 3.4.2
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.4.2)
* Fixes an issue when finalizing polls that has deleted options
* Updated translations

[1.21.0]
* Update Rallly to 3.5.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.5.0)

[1.21.1]
* Update Rallly to 3.5.1
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.5.1)

[1.22.0]
* Update Rallly to 3.6.1
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.6.0)
* Updated layout by @lukevella in https://github.com/lukevella/rallly/pull/976
* Add form validation feedback by @lukevella in https://github.com/lukevella/rallly/pull/998
* Listen on all interfaces when running in docker by @kolaente in https://github.com/lukevella/rallly/pull/1015
* Add plain text version for each email by @lukevella in https://github.com/lukevella/rallly/pull/1005
* Update input and focus styles by @lukevella in https://github.com/lukevella/rallly/pull/1012
* Allow user to customize the sender name for mails by @bmaster001 in https://github.com/lukevella/rallly/pull/1023
* Fixed an issue that caused redirect to login to fail when accessing the polls page.

[1.23.0]
* Update Rallly to 3.7.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.7.0)
* Use only geographic time zones by @lukevella in https://github.com/lukevella/rallly/pull/1033
* Fix timezone form input by @lukevella in https://github.com/lukevella/rallly/pull/1043
* Quicker UI updates when adding participant by @lukevella in https://github.com/lukevella/rallly/pull/1045
* Fix check for missing token on initial render by @lukevella in https://github.com/lukevella/rallly/pull/1054
* Fix issue where verification links would automatically get consumed by link checkers by @lukevella in https://github.com/lukevella/rallly/pull/1060

[1.24.0]
* Update Rallly to 3.8.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.8.0)

[1.24.1]
* Update Rallly to 3.8.1
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.8.1)

[1.24.2]
* Clear yarn cache

[1.25.0]
* Update Rallly to 3.9.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.9.0)

[1.25.1]
* Update Rallly to 3.9.1
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.9.1)
* Use infinite query on polls page by @lukevella in https://github.com/lukevella/rallly/pull/1200
* Remove squircle by @lukevella in https://github.com/lukevella/rallly/pull/1209
* Fix comments not deleted when deleting user by @lukevella in https://github.com/lukevella/rallly/pull/1214
* Fix crash when options change mid voting by @lukevella in https://github.com/lukevella/rallly/pull/1239
* Better error handling for browser time zone support by @lukevella in https://github.com/lukevella/rallly/pull/1240
* Fix fallback behaviour for unrecognized timezones by @lukevella in https://github.com/lukevella/rallly/pull/1241
* Add to calendar button by @lukevella in https://github.com/lukevella/rallly/pull/1225
* Update ghost button styles by @lukevella in https://github.com/lukevella/rallly/pull/1247
* Show Pro badge in sidebar by @lukevella in https://github.com/lukevella/rallly/pull/1251
* Close expanded poll with esc key by @lukevella in https://github.com/lukevella/rallly/pull/1253
* Detect time zone change by @lukevella in https://github.com/lukevella/rallly/pull/1254
* Delete all comments when deleting account by @lukevella in https://github.com/lukevella/rallly/pull/1267

[1.26.0]
* Update Rallly to 3.10.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.10.0)
* This release has updated email templates and adds support for localized email notifications.
* Updated transactional email templates by @lukevella in #1285
* Translations for Email Notifications by @lukevella in #1278
* Fix not found page not rendering properly by @lukevella in #1308
* Stylize detected timezone by @lukevella in #1311

[1.26.1]
* Update Rallly to 3.10.1
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.10.1)

[1.27.0]
* Update Rallly to 3.11.0
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.11.0)
* Use profile image from oauth provider by @lukevella in #1330
* Use avatar on login page by @lukevella in #1336
* Fix empty session when logging out by @lukevella in #1345
* Use node:20-slim as runner by @xavier-calland in #1350
* Update avatar colors by @lukevella in #1351
* Fix escaped values in emails by @lukevella in #1368
* fix ics download button by @a-mnich in #1381

[1.27.1]
* Update Rallly to 3.11.1
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.11.1)
* Fix event ICS download button file content

[1.27.2]
* Update Rallly to 3.11.2
* [Full changelog](https://github.com/lukevella/rallly/releases/tag/v3.11.2)
* Add support for Japanese language by @​lukevella in https://github.com/lukevella/rallly/pull/1401
* Fix issue with deleting user accounts with deleted polls by @​lukevella in https://github.com/lukevella/rallly/pull/1405

