FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/pkg /app/code

ENV NEXT_PUBLIC_SELF_HOSTED=true

ARG NODE_VERSION=20.15.0
RUN mkdir -p /usr/local/node-${NODE_VERSION} && curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH /usr/local/node-${NODE_VERSION}/bin:$PATH

# renovate: datasource=github-releases depName=lukevella/rallly versioning=semver extractVersion=^v(?<version>.+)$
ARG RALLLY_VERSION=3.11.2

# install dependencies, build and copy build output
# https://github.com/lukevella/rallly/blob/main/apps/web/Dockerfile#L43
RUN mkdir -p /app/code/build && cd /app/code/build && \
    wget https://github.com/lukevella/rallly/archive/refs/tags/v${RALLLY_VERSION}.tar.gz -O - | tar -xz --strip-components 1 -C /app/code/build && \
    yarn --frozen-lockfile --no-cache && \
    yarn global add turbo prisma && \
    yarn cache clean && \
    turbo db:generate && \
    SKIP_ENV_VALIDATION=1 turbo run build --filter=@rallly/web... && \
    # copy over build assets
    cp -R /app/code/build/apps/web/.next/standalone/. /app/code && \
    cp -R /app/code/build/apps/web/.next/static /app/code/apps/web/.next/ && \
    cp -R /app/code/build/apps/web/public /app/code/apps/web && \
    ln -s /app/data/env /app/code/apps/web/.env && \
    cp -rf /app/code/build/packages/database/prisma /app/pkg/ && \
    rm -rf /tmp/build /tmp/yarn* /tmp/v8-compile-cache*

COPY start.sh /app/pkg/

WORKDIR /app/pkg/
CMD [ "/app/pkg/start.sh" ]
