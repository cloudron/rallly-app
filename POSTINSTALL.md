Anyone can sign up and use the app. To restrict who can sign up, set `ALLOWED_EMAILS` in `/app/data/env` to the
list of allowed emails.

