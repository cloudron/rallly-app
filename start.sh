#!/bin/bash

set -eu

if [[ ! -f /app/data/env ]]; then
    echo "=> First run"

    # set random encryption key
    rallypwd="$(openssl rand -hex 32)"

    # https://github.com/lukevella/rallly/blob/main/.env.development
cat > /app/data/env <<EOT
SECRET_PASSWORD='${rallypwd}'
NEXTAUTH_SECRET='${rallypwd}'

# Comma separated list of email addresses that are allowed to register and login.
# You can use wildcard syntax to match a range of email addresses.
# Example: "john@example.com,jane@example.com" or "*@example.com"
ALLOWED_EMAILS=
EOT
fi

cd /app/code/apps/web

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "=> Setting up OIDC"

    export OIDC_NAME="Cloudron"
    export OIDC_DISCOVERY_URL="${CLOUDRON_OIDC_ISSUER}/.well-known/openid-configuration"
    export OIDC_CLIENT_ID="${CLOUDRON_OIDC_CLIENT_ID}"
    export OIDC_CLIENT_SECRET="${CLOUDRON_OIDC_CLIENT_SECRET}"
fi

export NEXT_PUBLIC_BASE_URL="${CLOUDRON_APP_ORIGIN}"
export NEXT_PUBLIC_APP_BASE_URL="${CLOUDRON_APP_ORIGIN}"
export NEXT_PUBLIC_SHORT_BASE_URL="${CLOUDRON_APP_ORIGIN}"
export NEXTAUTH_URL="${CLOUDRON_APP_ORIGIN}"
export DATABASE_URL="postgres://${CLOUDRON_POSTGRESQL_USERNAME}:${CLOUDRON_POSTGRESQL_PASSWORD}@${CLOUDRON_POSTGRESQL_HOST}:${CLOUDRON_POSTGRESQL_PORT}/${CLOUDRON_POSTGRESQL_DATABASE}"
export SUPPORT_EMAIL="${CLOUDRON_MAIL_FROM}"
export SMTP_HOST="${CLOUDRON_MAIL_SMTP_SERVER}"
export SMTP_PORT=${CLOUDRON_MAIL_SMTP_PORT}
export SMTP_SECURE=false
export SMTP_USER="${CLOUDRON_MAIL_SMTP_USERNAME}"
export SMTP_PWD="${CLOUDRON_MAIL_SMTP_PASSWORD}"

echo "=> Updating permissions"
chown -R cloudron:cloudron /app/data

# This requires the whole prisma/ folder not just the schema.prisma to exist
echo "=> prisma migrate"
gosu cloudron:cloudron prisma migrate deploy --schema=/app/pkg/prisma/schema.prisma

echo "=> Starting Rallly"
exec gosu cloudron:cloudron node  --dns-result-order=ipv4first ./server.js
