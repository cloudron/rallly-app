#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    let browser, app, shareLink;
    let athenticated_by_oidc = false;
    const USERNAME = process.env.USERNAME;
    const PASSWORD = process.env.PASSWORD;

    const pollTitle = 'Test Poll';

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }


    async function createAccount(displayName, email) {
        browser.manage().deleteAllCookies();

        // takes a bit for the app
        await browser.sleep(2000);

        await browser.get(`https://${app.fqdn}/register`);

        await waitForElement(By.id('name'));
        await browser.findElement(By.id('name')).sendKeys(displayName);
        await browser.findElement(By.id('email')).sendKeys(email);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();

        await waitForElement(By.xpath('//input[@name="code"]'));

        // code is sent via email and not stored in db. cannot test further
    }

    async function loginOIDC(username, password, justClickLogin = true) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/login`);

        await waitForElement(By.xpath('//button[contains(., "Login with Cloudron")]'));
        await browser.findElement(By.xpath('//button[contains(., "Login with Cloudron")]')).click();

        if (!justClickLogin) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await waitForElement(By.xpath('//h1[text()="Home"]'));
    }

    async function logout() {
        await browser.get('about:blank');
        await browser.get(`https://${app.fqdn}/settings/profile`);

        await waitForElement(By.xpath('//button[.="Logout"]'));
        await browser.findElement(By.xpath('//button[.="Logout"]')).click();

        await waitForElement(By.xpath('//div[text()="Login"]'));
    }

    async function createPoll() {
        await browser.get('about:blank');
        await browser.get('https://' + app.fqdn + '/new');

        await waitForElement(By.xpath('//input[@id="title"]'));
        await browser.findElement(By.xpath('//input[@id="title"]')).sendKeys(pollTitle);
        await browser.sleep(1000);

        await waitForElement(By.xpath('//span[text()="13"]'));
        await browser.findElement(By.xpath('//span[text()="13"]')).click();
        await browser.sleep(1000);

        await browser.findElement(By.xpath('//button[@type="submit"]')).click();

        await waitForElement(By.xpath('//a[contains(@href, "/invite")]'));
        shareLink = await browser.findElement(By.xpath('//a[contains(@href, "/invite")]')).getAttribute('href');
        console.log(`The shareLink is ${shareLink}`);
        if (!shareLink) throw new Error('Poll Share Link not found!');
    }

    async function checkPoll() {
        await browser.get(shareLink);
        await waitForElement(By.xpath(`//h1[text()="${pollTitle}"]`));
    }

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // no SSO . we can't check poll creation since it wants a code to login which is not in database and only in email
    it('install app (no SSO)', function () { execSync(`cloudron install --no-sso --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can create account', createAccount.bind(null, 'Jessie Jones', 'test@cloudron.io'));
    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    it('clear cache', clearCache);

    // SSO
    it('install app (SSO)', async function () {
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        console.log('wait for 10 seconds for app to really start');
        await browser.sleep(10000);
    });

    it('can get app information', getAppInfo);
    it('can login with OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false /* justClickLogin */));

    it('can create a new poll', createPoll);
    it('can logout', logout);

    it('check Poll', checkPoll);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can login with OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, true /* justClickLogin */));
    it('check Poll', checkPoll);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('check Poll', checkPoll);

    it('clear cache', clearCache);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);

        shareLink = shareLink.replace(LOCATION, `${LOCATION}2`);
        console.log(`new shareLink is ${shareLink}`);
    });
    it('can get app information', getAppInfo);
    it('can login with OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false /* justClickLogin */));
    it('can logout', logout);
    it('check Poll', checkPoll);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('clear cache', clearCache);

    it('can install app for update', async function () {
        execSync(`cloudron install --appstore-id ${app.manifest.id} --location ${LOCATION}`, EXEC_ARGS);
        console.log('wait for 10 seconds for app to really start');
        await browser.sleep(10000);
    });
    it('can get app information', getAppInfo);
    it('can login with OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false /* justClickLogin */));
    it('can create a new poll', createPoll);
    it('check Poll', checkPoll);
    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('check Poll', checkPoll);
    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});

